import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server: {
    host: '0.0.0.0',
    // port: 3000,
    proxy: {
      '/myapi': {
        target: 'https://222.173.243.142:7788',	//实际请求地址
        changeOrigin: true,
        secure: false,                          //安全证书校验
        rewrite: (path) => path.replace(/^\/myapi/, '')
      },
    }
  },
  // resolve: {
  //   alias: {
  //   // 关键代码
  //     '@': path.resolve(__dirname, './src')
  //   }
  // }
})
