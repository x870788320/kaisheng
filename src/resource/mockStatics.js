

export const mockStatics = {
    "dangerTotal": 1,
    "dh": 4445,
    "dl": 3,
    "docTotal": 7,
    "dt": 31,
    "dz": 391,
    "gc": 1220,
    "hiddenDangerDto": {
        "seriesDtos": [
            {
                "data": [
                    "0",
                    "0",
                    "1",
                    "2",
                    "1",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0"
                ],
                "name": "未整改",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "3",
                    "7",
                    "26",
                    "44",
                    "36",
                    "62",
                    "14",
                    "6",
                    "3",
                    "4"
                ],
                "name": "已整改",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0"
                ],
                "name": "驳回",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "1",
                    "0",
                    "0",
                    "0",
                    "0"
                ],
                "name": "已延期",
                "stack": "总量",
                "type": "bar"
            }
        ],
        "typeList": [
            "设备工程部",
            "动力车间",
            "K103车间",
            "K101车间",
            "K105车间",
            "K106车间",
            "K107车间",
            "储运部",
            "电仪自动化部",
            "安全管理部"
        ]
    },
    "jx": 4,
    "ld": 1232,
    "mb": 108,
    "noTotal": 734,
    "normalTotal": 1190,
    "riskDangerDto": {
        "seriesDtos": [
            {
                "data": [
                    "0",
                    "0",
                    "1",
                    "0",
                    "0"
                ],
                "name": "未整改",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "0",
                    "1",
                    "0",
                    "0",
                    "1"
                ],
                "name": "整改中",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "3",
                    "6",
                    "10",
                    "2",
                    "0"
                ],
                "name": "已验收",
                "stack": "总量",
                "type": "bar"
            },
            {
                "data": [
                    "0",
                    "0",
                    "2",
                    "0",
                    "0"
                ],
                "name": "已超期",
                "stack": "总量",
                "type": "bar"
            }
        ],
        "typeList": [
            "K201车间",
            "储运部",
            "K107车间",
            "K106车间",
            "动力车间"
        ]
    },
    "riskFour": 1,
    "riskOne": 1,
    "riskThree": 1,
    "riskTwo": 1,
    "sx": 424
}