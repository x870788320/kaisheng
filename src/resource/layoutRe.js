
//页面各模块的数据

export const leftLay = [
    {
        id: 1,
        name: 'risk',
        title:'风险分级管控',
        subtitle:'',
        key: '',
    },
    {
        id: 2,
        name: 'danger',
        title:'隐患排查',
        subtitle:'',
        key: '',
    },
    {
        id: 3,
        name: 'work',
        title:'特殊作业统计',
        subtitle:'Work',
        key: '',
    }
]


export const rightLay = [
    {
        id: 1,
        name: 'alarm',
        title:'报警分析',
        subtitle:'Alarm',
        key: '',
    }
]


export const bottomLay = [
    {
        id: 1,
        name: 'person',
        firTitle: '人员定位统计',
        title:'二道门内人数',
        subtitle:'Alarm',
        key: 'Personnel positioning',
    },
    {
        id: 2,
        name: 'legend',
        title:'系统图例',
        subtitle:'Legend',
        key: '',
    },
    {
        id: 3,
        name: 'rectificat',
        firTitle: '专项隐患整改',
        title:'风险点隐患整改',
        subtitle:'Special hidden danger rectification',
        key: '',
    },
]