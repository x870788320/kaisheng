
export const BotLeftSec = [
    {
        id: 1,
        name: '总人数',
        key: '2323'
    },
    {
        id: 2,
        name: '需充电',
        key: '2'
    },
]

//暂时的假数据
export const BotLeftThrD = [
    
    {
        id: 1,
        name: '采购部',
        key: '9'
    },
    {
        id: 2,
        name: '质量部',
        key: '19'
    },
    {
        id: 3,
        name: '办公室',
        key: '0'
    },
    {
        id: 4,
        name: '动力车间',
        key: '6'
    },
    {
        id: 5,
        name: '103车间',
        key: '23'
    },
    {
        id: 6,
        name: '101车间',
        key: '21'
    },
    {
        id: 7,
        name: '业务部',
        key: '8'
    },
    {
        id: 8,
        name: '102车间',
        key: '14'
    },
    {
        id: 9,
        name: '工程部',
        key: '7'
    },
]

export const botCenterData = [
    {
        id: 1,
        name: '一级',
        icon: 'level01.png',
        key: ''
    },
    {
        id: 2,
        name: '二级',
        icon: 'level02.png',
        key: ''
    },
    {
        id: 3,
        name: '监测点位',
        icon: 'monitorP.png',
        key: ''
    },
    {
        id: 4,
        name: '三级',
        icon: 'level03.png',
        key: ''
    },
    {
        id: 5,
        name: '四级',
        icon: 'level04.png',
        key: ''
    },
    {
        id: 6,
        name: '报警点位',
        icon: 'alarmP.png',
        key: ''
    },
    {
        id: 7,
        name: '液位',
        icon: 'ye_p.png',
        key: ''
    },
    {
        id: 8,
        name: '压力',
        icon: 'yl_p.png',
        key: ''
    },
    {
        id: 9,
        name: '布控球',
        icon: 'bkq_p.png',
        key: ''
    },
    {
        id: 10,
        name: '温度',
        icon: 'tem_p.png',
        key: ''
    },
    {
        id: 11,
        name: '气体分析',
        icon: 'gas_p.png',
        key: ''
    },
    {id: 12}
]