
// 风险分级管控
export const riskArrData = [
    {
        id: 1,
        name:'一级',
        icon: 'fx-I.png',
        key:'riskOne'
    },
    {
        id: 2,
        name:'二级',
        icon: 'fx-II.png',
        key:'riskTwo'
    },
    {
        id: 3,
        name:'三级',
        icon: 'fx-III.png',
        key:'riskThree'
    },
    {
        id: 4,
        name:'四级',
        icon: 'fx-IV.png',
        key:'riskFour'
    },
]

//隐患排查
export const dangerArrData = [
    {
        id: 1,
        name: '正常',
        icon: 'yh-normal.png',
        key: 'normalTotal'
    },
    {
        id: 2,
        name: '存在隐患',
        icon: 'yh-warning.png',
        key: 'dangerTotal'
    },
    {
        id: 3,
        name: '未排查',
        icon: 'yh-yet.png',
        key: 'noTotal'
    },
]

//特殊作业显示数据
export const workArrData = [
    {
        id: 1,
        title: '动火',
        icon: 'icon-dh.png',
        key: 'dh'
    },
    {
        id: 2,
        title: '高处',
        icon: 'icon-gc.png',
        key: 'gc'
    },
    {
        id: 3,
        title: '受限',
        icon: 'icon-sx.png',
        key: 'sx'
    },
    {
        id: 4,
        title: '临电',
        icon: 'icon-ld.png',
        key: 'ld'
    },
    {
        id: 5,
        title: '盲板',
        icon: 'icon-mb.png',
        key: 'mb'
    },
    {
        id: 6,
        title: '动土',
        icon: 'icon-dt.png',
        key: 'dt'
    },
    {
        id: 7,
        title: '断路',
        icon: 'icon-dl.png',
        key: 'dl'
    },
    {
        id: 8,
        title: '吊装',
        icon: 'icon-dz.png',
        key: 'dz'
    },
    {
        id: 9,
        title: '检维修',
        icon: 'icon-jwx.png',
        key: 'jx'
    },
]