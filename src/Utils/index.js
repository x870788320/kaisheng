

//日期时间
const formatNum = n => n.toString()[1] ? n : '0' + n
const weekDays = ['Mon.', 'Tues.', 'Wed.', 'Thur.', 'Fri.', 'Sat.', '七Sun.']
export const formatDate = time => {
    let date = time || new Date()
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    let day = date.getDate()
    let weekDay = weekDays[date.getDay() -1]
    let hour = date.getHours()
    let minute = date.getMinutes()
    let second = date.getSeconds()
    return [ [ month, day].map(formatNum).join('-'), weekDay, [hour, minute, second].map(formatNum).join(':') ]
    // return [ month, day].map(formatNum).join('-') +'   ' + weekDay + ' ' +[hour, minute, second].map(formatNum).join(':')
}

//这里引入不生效，需要复制到组件
const formanSrc = src =>  new URL(src, import.meta.url).href


//获取范围内随机数
export const randomNum = (start, end) => parseInt(Math.random() * (end - start + 1) + start);