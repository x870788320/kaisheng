// import * as THREE from 'three'

import { Scene, Clock, LoadingManager, Raycaster } from 'three'

//模型加载器
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';

import { initRenderer, initDefaultLighting, initCamera, initStats, initControls, initPMREM } from './threePrepare'
import { defaultFacadePayload } from './threeResource'


var rayCaster   = new Raycaster();

export default class Facade{
    constructor(){
        this.stats = null
        this.HTTP = null
        this.scene = null
        this.camera = null
        this.renderer = null
        this.clock = null
        this.controls = null
        // this.light = null
        this.labelManger = null
        // this.cube = null
        this.cameralResetData = null
    }

    init(payload, cameraCfg){
        const { axios, container, devicePixelRatio, width,height } = { ...defaultFacadePayload, ...payload }
        //FPS MS MB
        this.stats = initStats();
        this.HTTP = axios
        //场景
        this.scene = new Scene()
        //用于跟踪时间的对象。如果性能可用
        this.clock = new Clock;
        this.camera = initCamera({ width, height, cameraCfg})
        this.renderer = initRenderer({ width, height, container })
        // 环境光、光源
        // initDefaultLighting(this.scene)


        this.controls = initControls(this.camera, this.renderer.domElement, cameraCfg)

        initPMREM(this.scene, this.renderer)

        //用来reset
        this.cameralResetData = {
            VecCameraP: this.camera.position.clone(), //摄像机原始位置,
            VecLookP: this.controls.target.clone() //目标向量,
        } 
        // this.camera.position.clone(); //摄像机原始位置
        // this.cameralInitTarget = this.controls.target.clone(); //目标向量

        //窗口大小变化监听
        window.addEventListener('resize',() => {
            let w = window.innerWidth 
            let h = window.innerHeight
            this.camera.aspect = w / h;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(w,h)
            if(this.labelRenderer) this.labelRenderer.setSize(w, h);
        },false);

        window.addEventListener('dblclick', event => {
            console.log('camera.position----',this.camera.position);
            console.log('controls.target----',this.controls.target);
            
            let mouse = {}
            mouse.x = (event.clientX / window.viewWidth) * 2 - 1;
            mouse.y = - (event.clientY / window.viewHeight) * 2 + 1;

            rayCaster.setFromCamera(mouse, this.camera);
            var intersects = rayCaster.intersectObjects(this.scene.children, true);
            console.log(this.scene.children)
            console.log(intersects)

            if (intersects.length > 0) {

                var object = intersects[0].object;
                var point  = intersects[0].point;

                console.log("=====position=====",point);
                console.log("object:",object);
            }
        });
    }

    //开始动
    updateView(){
        const { stats, controls, renderer, labelRenderer, scene, camera } = this
        // window.requestAnimationFrame(this.updateView);
        if (stats) stats.update();
        if (controls) this.controls.update();
        if (renderer) this.renderer.render(scene, camera);
        if (labelRenderer) this.labelRenderer.render(scene, camera);
    }

    //加载模型 
    loadModels(srcs, onModelLoad){
        if(!srcs) return
        //模型加载管理器， 可以获取到加载进度
        let manager = new LoadingManager();
        // 模型加载器
        let gltfLoader  = new GLTFLoader(manager);
        //用于处理压缩的文件
        let dracoLoader = new DRACOLoader();
        dracoLoader.setDecoderPath('./gltfdraco/');
        gltfLoader.setDRACOLoader(dracoLoader);

        manager.onLoad = onModelLoad;
        srcs.map( item => {
            gltfLoader.load(item, gltf => {
                this.scene.add(gltf.scene);
                //获取场景
                const scene = gltf.scene;
                //获取某个模型
                const mesh = scene.children[ 0 ];
                //获取到模型上的数据
                const fooExtension = mesh.userData;
                //获取模型上的信息
                console.log('fooExtension', fooExtension)

                
                // 环境光、光源
                initDefaultLighting(this.scene)

                // gltf.scene.traverse( function ( child ) {
                //     if ( child.isMesh ) {
                //         child.castShadow = true;
                //         // console.log(child)
                //       child.material.emissive =  child.material.color;
                //       child.material.emissiveMap = child.material.map ;
                //     }
                    
                //     if( 'Mesh019_20' == child.name || 'Mesh019_19' == child.name  ){
                //         console.log(child)
                //         child.receiveShadow = true
                //         // child.material.emissive =  child.material.color;
                //         // child.material.emissiveMap = child.material.map ;
                //     }
                //   });
            });
        })
    }

    //观看视角
    lookAt(target){
        this.controls.target = target;
    }

    // 保存模型颜色
    saveObjState(){
        this.scene.traverse(obj=>{
            if(obj.material){
                if(!obj.oldMaterial){
                    obj.oldMaterial = {color:obj.material.color,map:obj.material.map};
                }
            }
        });
    }
    
    reset(){
        const { VecCameraP, VecLookP } = this.cameralResetData
        this.controls.target = VecLookP;
        this.camera.position.set( VecCameraP.x, VecCameraP.y, VecCameraP.z );
    }
}