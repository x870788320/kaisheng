
//配置项默认值
export const defaultFacadePayload = {
    axios : null,                        // ajax axios
    container : null,                      // 容器dom
    devicePixelRatio: window.devicePixelRatio,   //像素比
    width: window.innerWidth,                   // 屏幕大小 
    height: window.innerHeight
} 

 //相机的初始化数据参数
// export const cameraDef = {
//     perspectiveCfg:{
//         range: 60,
//         near: 1,
//         far: 10000
//     },
//     cameraP: {
//         x: 1000,
//         y: 1000,
//         z: 1000
//     },
//     lookP: {
//         x: 0,
//         y: 0,
//         z: 0
//     },
// }
//相机的初始化数据参数
// export const cameraDef = {
//    perspectiveCfg:{
//        range: 60,
//        near: 10,
//        far: 100000
//    },
//    cameraP: {
//        x: -24.850682642522067,
//        y: 123.5567609584884,
//        z: 161.57806404751557
//    },
//    lookP: { 
//        x: 0.04675571193766753,
//        y: -37.111081976162,
//        z: 18.704356341191176
//    },
// }

//相机的初始化数据参数
export const cameraDef = {
    perspectiveCfg:{
        range: 60,
        near: 10,
        far: 100000
    },
    cameraP: {
        "x": 65.41407103990316,
        "y": 230.64630751861625,
        "z": 281.6264000674835
    },
    lookP: {
        "x": 58.873459923325996,
        "y": -76.00988032680624,
        "z": -7.67080652403127
    },
 }