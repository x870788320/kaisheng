
import {
    PerspectiveCamera,
    Color,
    WebGLRenderer,
    AmbientLight,
    Vector3,
    PMREMGenerator,
    UnsignedByteType,
    SpotLight,
    PCFSoftShadowMap,
    HemisphereLight,
    DirectionalLight,
    PointLight,
    MeshLambertMaterial,
    DoubleSide,
    PlaneGeometry,
    Mesh,
    SphereGeometry
} from 'three';

import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import {RGBELoader} from 'three/examples/jsm/loaders/RGBELoader';

import { Stats } from './Stats'

import { cameraDef } from './threeResource'


//初始化渲染器
export const initRenderer = pyload => {
    console.log('pyload', pyload)
    const renderer = new WebGLRenderer({antialias: true, alpha: true});
    renderer.shadowMap.enabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMap.type = PCFSoftShadowMap;

    renderer.setClearColor(new Color(0x030E1B));
    renderer.setSize(pyload.width, pyload.height);
    // renderer.shadowMap.enabled = true;
    console.log(pyload.container)
    pyload.container.appendChild(renderer.domElement);
    return renderer
}

//初始化默认灯光
export const initDefaultLighting = scene => {
    //环境光
    scene.add(new AmbientLight(0xffffff))

    // 添加聚光灯
    const spotLight = new SpotLight(0xffffff)
    spotLight.position.set(0, 600, 0)
    spotLight.castShadow = true
    //   scene.add(spotLight) // 聚光灯添加到场景中

    const hemiLight = new HemisphereLight(0xffffff, 0x000000, 0.6);
    // hemiLight.position.set(0, 600, 0);
    // scene.add(hemiLight);

    
    let directionalLight = new DirectionalLight( 0xffffff, 3);
    directionalLight.position.set( -500, 800, 500 );

    console.log(scene)
    directionalLight.castShadow = true; // default false
    console.log(scene.getObjectByName('Cube'))
    console.log(scene)
    directionalLight.target = scene.getObjectByName('Cube'); // default false
    directionalLight.position.set( 900, 900, 900 );
    directionalLight.castShadow = true; // default false
    
    directionalLight.shadow.mapSize.width = 2048;    // 定义阴影贴图的宽度和高度,必须为2的整数此幂
    directionalLight.shadow.mapSize.height = 2048;   // 较高的值会以计算时间为代价提供更好的阴影质量
    directionalLight.shadow.camera.near = 20;
    directionalLight.shadow.camera.far = 1800;

    directionalLight.shadow.camera.left = -500;
    directionalLight.shadow.camera.right = 500;
    directionalLight.shadow.camera.top = 500;
    directionalLight.shadow.camera.bottom = -500;
    scene.add( directionalLight );

    let pointLight = new PointLight(0x666666, 3)
    pointLight.position.set(100, 200, 100)
    pointLight.castShadow = true;
    scene.add(pointLight)

}

export const addSpotLight = (vizbim) => {
    vizbim.renderer.shadowMap.enabled = true;  // 将渲染器的阴影设置打开，这样渲染器就可以渲染场景内物体的阴影了
  
    const spotLight = new SpotLight(0xffffff, 1);  // 实例化一个spotlight聚光灯
    // 将聚光灯的位置调整到正方体左上方一点
    spotLight.position.copy(0, 200, 0);  // 将聚光灯的位置设置为正方体的位置
    // spotLight.position.z += 300;  // 将聚光灯的位置向z轴方向调整300
    // spotLight.position.x += 200;
  
    // spotLight.angle = Math.PI / 4; // 聚光灯的扩散范围，最大为Math.PI / 2
    // spotLight.penumbra = 0.05;  // 由于半影而衰减的聚光锥的百分比。取值介于0和1之间。默认值为零
    spotLight.decay = 2;  // 光线沿光线距离变暗的量, 可理解为衰减速率
    spotLight.distance = 3000; // 最大范围的光
  
    spotLight.castShadow = true;  // 将光源能产生阴影的属性设置为true
    spotLight.shadow.mapSize.width = 1024;    // 定义阴影贴图的宽度和高度,必须为2的整数此幂
    spotLight.shadow.mapSize.height = 1024;   // 较高的值会以计算时间为代价提供更好的阴影质量
    spotLight.shadow.camera.near = 100;     // 光源在世界坐标内的视角，物体的位置必须大于此最小值才能产生阴影
    spotLight.shadow.camera.far = 2400;
    // spotLight.target = vizbim.components[componentid];  // Spotlight从其位置指向target.position。将聚光灯的方向设置为正方体的位置
  
    // vizbim.components[componentid].castShadow = true;  // 将正方体产生阴影的设置属性打开
  
    // addGround(vizbim); // 创建一个矩形模仿地面，用来接收正方体的阴影
    qiu(vizbim)
    // vizbim.scene.add(spotLight);  // 将聚光灯添加到场景里
  }

  const addGround = (vizbim) => {
    const material = new MeshLambertMaterial({
      side: DoubleSide,  // 将材质设置为双面，如果设置为单面，则只有一面能看见材质，另一面看着就是透明
    });
    const geometry = new PlaneGeometry(500, 500, 32);  // 一个长方形几何体，长宽都为100
    const cube = new Mesh(geometry, material);  // 创建这个mesh对象
    // cube.position.copy(vizbim.components[componentid].position);  // 将这个矩形的位置设置为正方体物体的位置
    // cube.position.z -= 600;  // 将这个正方体的位置向z轴负方向调整600
    // cube.position.x -= 600;  // 将这个正方体的位置向z轴负方向调整600
    cube.position.y -= 50;  // 将这个正方体的位置向z轴负方向调整600
    cube.rotateX(Math.PI/2) 
    cube.receiveShadow = true;    // 将地面接收阴影的属性打开
    vizbim.scene.add(cube);  // 将地面添加到场景中
    // moveTheBox();
  }

  const qiu = (scene) => {
      //球体
        var sphereGeometry = new SphereGeometry(20,40,50);
        var sphereMaterial = new MeshLambertMaterial({
            color:0x777777,
            wireframe: true
        });
        var sphere = new Mesh(sphereGeometry,sphereMaterial);
        sphere.castShadow  = true;
        sphere.position.x = 10;  sphere.position.y = 10;  sphere.position.z = 100;
        var step = 0;
        sphere.onBeforeRender = function(){
            step += 0.04;
            sphere.position.x = 20 + (10*(Math.cos(step)));
            sphere.position.y = 20 + (10*Math.abs(Math.sin(step)));
        }

        scene.add(sphere)
  }

//初始化相机
export const initCamera = params => {
    console.log('params', params)
    const { width, height, cameraCfg = {} } = params
    const { perspectiveCfg, cameraP } = { ...cameraDef, ...cameraCfg }
    // // PerspectiveCamera 透视摄像机
    let camera = new PerspectiveCamera(perspectiveCfg.range, width / height, perspectiveCfg.near, perspectiveCfg.far);
    camera.position.set(cameraP.x, cameraP.y, cameraP.z);
    //lookat与control.tartget冲突时，以control为准
    // camera.lookAt(new THREE.Vector3(lookP.x, lookP.y, lookP.z));
    return camera;
}


export const initControls = (camera, dom, cameraCfg) => {
    //定义控制器核心           
    
    const { lookP } = { ...cameraDef, ...cameraCfg }
    let obtControls = new OrbitControls(camera, dom);

    // 如果使用animate方法时，将此函数删除
    // controls.addEventListener('change', render);
    //以下都是为了满足各种需求的各种控制器配置参数
    obtControls.enableDampling = true; //使动画循环使用时阻尼或自转 意思是否有惯性
    obtControls.enableZoom = true; //是否允许缩放
    obtControls.enablePan = true; //是否开启鼠标右键拖拽

    obtControls.autoRotate = false; //是否允许自动旋转
    obtControls.dampingFactor = 0.25; //动态阻尼系数：就是鼠标拖拽旋转灵敏度
    //惯性速度
    obtControls.rotateSpeed = 0.3;
    //平移速度
    obtControls.panSpeed = 0.5;
    //使用键盘时相机的平移速度。默认值为每个按键7.0像素。
    obtControls.keyPanSpeed = 7.0;	// pixels moved per arrow key push

    //垂直轨道的距离，上限。数学的范围是0。PI弧度，默认值为Math。圆周率。
    obtControls.maxPolarAngle = Math.PI / 2.2;

    obtControls.minDistance = 10; //设置相机距离原点的最近距离；
    obtControls.maxDistance = 1600; //设置相机距离原点的最远距离；

    obtControls.target = new Vector3(lookP.x, lookP.y, lookP.z);
    return obtControls
}

//添加HDR贴图
export const initPMREM = ( scene, renderer ) => {
    //创建一个新的 PMREM 生成器  从 cubeMap 环境纹理生成预过滤的 Mipmapped 辐射环境贴图 （PMREM）
    let pmremGenerator = new PMREMGenerator(renderer);
    //预编译等矩形着色器。通过在纹理的网络获取过程中调用此方法以提高并发性，可以更快地启动。
    pmremGenerator.compileEquirectangularShader();

    //用于加载二进制文件格式的(rgbe, hdr, …)的抽象类
    new RGBELoader().setDataType(UnsignedByteType).load('@/modelLoader/textures/pump_station_1k.hdr', texture => {
        console.log(texture)
        //Texture  纹理
        //等距柱状投影 – 等距柱状投影纹理。从等距柱状投影纹理生成 PMREM，该纹理可以是 LDR （RGBFormat） 或 HDR （RGBEFormat）。理想的输入图像大小为 1k （1024 x 512），因为这与 256 x 256 立方体贴图输出最匹配。
        let envMap = pmremGenerator.fromEquirectangular(texture).texture;
        pmremGenerator.dispose();

        console.log(envMap)
        //环境
        scene.environment = envMap; // 给场景添加环境光效果
        // scene.background = envMap; // 给场景添加背景图
    });
}


//性能监测插件
export const initStats = type => {

    var panelType = (typeof type !== 'undefined' && type) && (!isNaN(type)) ? parseInt(type) : 0;
    var stats = new Stats();

    stats.showPanel(panelType); // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild(stats.dom);

    return stats;
}